package multicados.master.config;

import nh.core.domain.DomainResource;
import nh.core.domain.Entity;
import nh.core.domain.graph.DomainResourceGraph;
import nh.core.domain.sf.SessionFactoryEntry;
import nh.core.domain.sf.SessionFactoryEntryRegistrar;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.springframework.context.ApplicationContext;

/**
 * @author Ngoc Huy
 */
@SuppressWarnings({"rawtypes", "unused"})
public class HibernateSessionFactoryEntry
    implements SessionFactoryEntryRegistrar<Entity> {

  @Override
  public SessionFactoryEntry<Entity> register(
      ApplicationContext applicationContext,
      DomainResourceGraph<DomainResource> resourceGraph) {
    return new SessionFactoryEntry<>() {

      @Override
      public SessionFactoryImplementor getSessionFactory() {
        return applicationContext.getBean(SessionFactory.class)
            .unwrap(SessionFactoryImplementor.class);
      }

      @Override
      public DomainResourceGraph<Entity> getGraph() {
        return resourceGraph.locate(Entity.class);
      }

    };
  }

}
