package multicados.master.config;

import nh.core.config.DomainResourceContextConfiguration;
import nh.core.config.DomainSecurityContextConfiguration;
import nh.core.config.GenericRepositoryConfiguration;
import nh.core.config.HibernateConfiguration;
import nh.core.domain.validation.DomainValidationConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Ngoc Huy
 *
 */
@Configuration
@Import({
    HibernateConfiguration.class,
    DomainResourceContextConfiguration.class,
    DomainSecurityContextConfiguration.class,
    GenericRepositoryConfiguration.class,
    DomainValidationConfiguration.class})
public class CoreImportConfiguration {

}
