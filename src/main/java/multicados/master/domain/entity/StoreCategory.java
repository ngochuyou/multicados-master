package multicados.master.domain.entity;

import static jakarta.persistence.FetchType.LAZY;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.Table;
import multicados.common.domain.AuditablePermanentEntity;
import multicados.common.domain.definitions.MysqlType;
import multicados.common.domain.mapping.Identifiable;
import multicados.master.domain.entity.id.StoreCategoryId;
import multicados.master.domain.entity.id.StoreCategoryId_;
import multicados.master.domain.entity.mapping.StoreCategoryMapping;

@Entity
@Table(name = "stores_categories")
public class StoreCategory extends AuditablePermanentEntity<StoreCategoryId> {

  @EmbeddedId
  private StoreCategoryId id;

  @ManyToOne(optional = false, fetch = LAZY)
  @JoinColumn(name = StoreCategoryMapping.STORE_ID, referencedColumnName = Identifiable.ID, columnDefinition = MysqlType.BIGINT, nullable = false, updatable = false)
  @MapsId(StoreCategoryId_.STORE_ID)
  private Store store;

  @ManyToOne(optional = false, fetch = LAZY)
  @JoinColumn(name = StoreCategoryMapping.CATEGORY_ID, referencedColumnName = Identifiable.ID, columnDefinition = MysqlType.BIGINT, nullable = false, updatable = false)
  @MapsId(StoreCategoryId_.CATEGORY_ID)
  private Category category;

  @Override
  public StoreCategoryId getId() {
    return id;
  }

  @Override
  public void setId(StoreCategoryId id) {
    this.id = id;
  }
}
