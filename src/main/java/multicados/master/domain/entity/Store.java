package multicados.master.domain.entity;

import static multicados.common.domain.definitions.Generator.TABLE_GENERATOR;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.TableGenerator;
import java.math.BigInteger;
import multicados.common.domain.AuditablePermanentEntity;
import multicados.common.domain.definitions.Code;
import multicados.common.domain.definitions.Generator;
import multicados.common.domain.definitions.MysqlType;
import nh.core.domain.EncryptedIdentifierResource;
import nh.core.domain.NamedResource;
import nh.core.domain.annotation.Encrypted;
import nh.core.domain.annotation.Name;

@Entity
@Table(name = "stores")
public class Store extends AuditablePermanentEntity<BigInteger>
  implements NamedResource, EncryptedIdentifierResource<BigInteger, String> {

  @Id
  @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_GENERATOR)
  @TableGenerator(name = TABLE_GENERATOR, initialValue = Generator.Base32.CROCKFORD_10A
      - 1, allocationSize = 1, table = Generator.TABLE_GENERATOR_TABLE_NAME)
  @Column(updatable = false, columnDefinition = MysqlType.BIGINT)
  private BigInteger id;

  @Encrypted
  @Column(unique = true, length = Code.LENGTH)
  private String code;

  @Name
  @Column(nullable = false)
  private String name;

  @Override
  public BigInteger getId() {
    return id;
  }

  @Override
  public void setId(BigInteger id) {
    this.id = id;
  }

  @Override
  public String getCode() {
    return code;
  }

  @Override
  public void setCode(String code) {
    this.code = code;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }
}
