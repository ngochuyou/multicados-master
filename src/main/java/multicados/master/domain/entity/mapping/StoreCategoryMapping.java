package multicados.master.domain.entity.mapping;

public abstract class StoreCategoryMapping {

  private StoreCategoryMapping() {
    throw new UnsupportedOperationException();
  }

  public static final String CATEGORY_ID = "category_id";
  public static final String STORE_ID = "store_id";
  public static final String ASSIGNED_ON = "assigned_on";

}
