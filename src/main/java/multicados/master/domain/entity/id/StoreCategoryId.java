package multicados.master.domain.entity.id;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Objects;
import multicados.common.domain.definitions.MysqlType;
import multicados.master.domain.entity.mapping.StoreCategoryMapping;

@Embeddable
public class StoreCategoryId implements Serializable {

  @Column(name = StoreCategoryMapping.CATEGORY_ID, columnDefinition = MysqlType.BIGINT)
  private BigInteger categoryId;

  @Column(name = StoreCategoryMapping.STORE_ID, columnDefinition = MysqlType.BIGINT)
  private BigInteger storeId;

  @Column(name = StoreCategoryMapping.ASSIGNED_ON, nullable = false, updatable = false)
  private LocalDate assignedOn;

  public StoreCategoryId() {
  }

  public StoreCategoryId(BigInteger categoryId, BigInteger storeId, LocalDate assignedOn) {
    this.categoryId = categoryId;
    this.storeId = storeId;
    this.assignedOn = assignedOn;
  }

  public BigInteger getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(BigInteger categoryId) {
    this.categoryId = categoryId;
  }

  public BigInteger getStoreId() {
    return storeId;
  }

  public void setStoreId(BigInteger storeId) {
    this.storeId = storeId;
  }

  public LocalDate getAssignedOn() {
    return assignedOn;
  }

  public void setAssignedOn(LocalDate assignedOn) {
    this.assignedOn = assignedOn;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    StoreCategoryId that = (StoreCategoryId) o;

    if (!Objects.equals(categoryId, that.categoryId)) {
      return false;
    }
    if (!Objects.equals(storeId, that.storeId)) {
      return false;
    }
    return Objects.equals(assignedOn, that.assignedOn);
  }

  @Override
  public int hashCode() {
    int result = categoryId != null ? categoryId.hashCode() : 0;
    result = 31 * result + (storeId != null ? storeId.hashCode() : 0);
    result = 31 * result + (assignedOn != null ? assignedOn.hashCode() : 0);
    return result;
  }
}
