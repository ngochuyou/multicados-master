/**
 *
 */
package multicados.master.constants;

/**
 * @author Ngoc Huy
 */
public abstract class Settings {

  public static final String BASE_PACKAGE = "multicados.master";

  private Settings() {
    throw new UnsupportedOperationException();
  }

}
