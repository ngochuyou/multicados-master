/**
 *
 */
package multicados.master.application;

import static multicados.master.constants.Settings.BASE_PACKAGE;

import java.util.TimeZone;
import nh.core.constants.Settings;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

/**
 * @author Ngoc Huy
 *
 */
@SpringBootApplication(exclude = {
    HibernateJpaAutoConfiguration.class,
    RedisRepositoriesAutoConfiguration.class,
    RedisAutoConfiguration.class
}, scanBasePackages = BASE_PACKAGE)
@ConfigurationPropertiesScan({
    BASE_PACKAGE, Settings.CORE_PACKAGE
})
public class MulticadosMaster {

  public static void main(String[] args) {
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    SpringApplication.run(MulticadosMaster.class, args);
  }

}
